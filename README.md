[![Code Sponsor](https://app.codesponsor.io/embed/jtMnZHip717gzMbuPAbeqRkC/catalinmiron/mobile-shopping-react-native.svg)](https://app.codesponsor.io/link/jtMnZHip717gzMbuPAbeqRkC/catalinmiron/mobile-shopping-react-native)

![image](mobile_shopping_inspiration.gif)

----

Stack: 

- React native
- Exponent


----

![Aristotle](http://images.gr-assets.com/authors/1390143800p2/2192.jpg)
> “For the things we have to learn before we can do them, we learn by doing them.” 

― Aristotle, The Nicomachean Ethics
